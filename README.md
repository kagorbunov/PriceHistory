# Golang final exam
## Transaction Price History Service
## Task №2
Postman description :
https://documenter.getpostman.com/view/19090990/VUjSGixJ

Postman description JSON :
https://www.postman.com/collections/81203f5c65946e716205
## Run locally


Please run the services in order.
1) Start task №1
2) Start task №2
3) Start task №3
- Build:

```bash
$ git clone https://gitlab.com/kagorbunov/PriceHistory.git
$ cd PriceHistory
$ go mod download

```
- Start postgres:
```bash
$ sudo make postgres
```
- Goose migration:
```bash
$ sudo make migration
```

- Run:
```bash 
$ sudo make run
```

- Check lint:

```bash
$ sudo make lint-run
```

Server is listening on http://localhost:8070

[GET] http://localhost:8060/ohlc

[GET] http://localhost:8060/ohlc/name/{name}

[GET] http://localhost:8060/ohlc/timeframe/{timeframe}

[GET] http://localhost:8060/ohlc/{name}/{timeframe}

All configuration parameters in .env