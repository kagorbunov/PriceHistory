package main

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"gitlab.com/kagorbunov/PriceHistory/pkg/config"
	"gitlab.com/kagorbunov/PriceHistory/pkg/handler"
	"gitlab.com/kagorbunov/PriceHistory/pkg/repository"
	"gitlab.com/kagorbunov/PriceHistory/services/manager"
	"log"
	"net/http"
	"time"
)

const appName = "service.PriceHistory"

func main() {
	fmt.Println("Project Name: ", appName)

	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	cfg := config.New()

	postgres := repository.NewPg(cfg.DB.Username, cfg.DB.Password, cfg.DB.DBName, cfg.DB.Addr)
	router := mux.NewRouter()
	h := handler.NewHandler(ctx, router, postgres)
	h.InitializeRoutes()
	m := manager.NewManager(postgres, cfg.CurrencyPairs.AddrAll)
	err := m.CreateCurrencyPairs()
	if err != nil {
		log.Fatalf("Error creating currency pairs: %v", err)
	}

	go func(postgres *repository.Pg, url, timeout string) {
		for {
			handler.DoRequest(postgres, url)

			t, err := time.ParseDuration(timeout)
			if err != nil {
				log.Fatalf("Error parsing time: %v", err)
			}
			time.Sleep(t)
		}
	}(postgres, cfg.CurrencyPairs.Addr, cfg.CurrencyPairs.Timeout)

	log.Fatal(http.ListenAndServe(cfg.Router.Addr, router))
}
