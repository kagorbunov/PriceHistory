package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/kagorbunov/PriceHistory/pkg/repository"
	"net/http"
	"time"
)

type Handler struct {
	ctx    context.Context
	Router *mux.Router
	Pg     *repository.Pg
}

type responseTranPeriod struct {
	Name      string `json:"name"`
	StartData string `json:"start_data"`
	StopData  string `json:"stop_data"`
}

func NewHandler(ctx context.Context, Router *mux.Router, Pg *repository.Pg) *Handler {
	return &Handler{ctx: ctx, Router: Router, Pg: Pg}
}
func (h *Handler) InitializeRoutes() {
	h.Router.HandleFunc("/transaction_history", h.GetTransactionHistory).Methods("POST")
}

func (h *Handler) GetTransactionHistory(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var respTP responseTranPeriod
	cPair := json.NewDecoder(r.Body)
	if err := cPair.Decode(&respTP); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	defer r.Body.Close()

	startData, err := time.Parse(time.Stamp, respTP.StartData)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, fmt.Sprintf("Error with parse start Data: %s", vars))
	}
	stopData, err := time.Parse(time.Stamp, respTP.StopData)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, fmt.Sprintf("Error with parse stop Data: %s", err))
	}

	cPairs, err := h.Pg.GetPriceOfTransactionsInPeriod(respTP.Name, startData, stopData)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, fmt.Sprintf("Error with get record from db: %s", err))
	}
	responseTransaction := newResponseTransaction(respTP.Name, cPairs)
	respondWithJSON(w, http.StatusOK, responseTransaction)
}
