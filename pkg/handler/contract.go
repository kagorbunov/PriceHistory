package handler

import "gitlab.com/kagorbunov/PriceHistory/pkg/repository"

type ResponseTransaction struct {
	Name  string        `json:"name"`
	Value []transaction `json:"value"`
}

func newResponseTransaction(name string, pt []repository.PriceOfTransactions) *ResponseTransaction {
	var value []transaction
	for _, el := range pt {
		val := transaction{Data: el.Data, Price: el.Price}
		value = append(value, val)
	}
	return &ResponseTransaction{Name: name, Value: value}
}

type transaction struct {
	Data  string `json:"data"`
	Price string `json:"price"`
}
