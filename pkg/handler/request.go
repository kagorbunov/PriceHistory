package handler

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"gitlab.com/kagorbunov/PriceHistory/pkg/repository"
	"log"
	"net/http"
)

type responsePriceGenerator struct {
	Name   string           `json:"currency_pair"`
	Values []listPriceValue `json:"value"`
}

type listPriceValue struct {
	Data  string `json:"data"`
	Price string `json:"price"`
}

func MakeRequest(url, currency string) *responsePriceGenerator {
	resp, err := http.Get(fmt.Sprintf("%v%v", url, currency))
	if err != nil {
		log.Fatalln(err)
		return nil
	}
	defer resp.Body.Close()
	var result *responsePriceGenerator

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		log.Fatalln(err)
		return nil
	}

	return result
}

func DoRequest(pg *repository.Pg, url string) {
	listCurrency, err := pg.GetAllCurrencyPairs()
	if err != nil {
		log.Fatalf("Error getting all currencies: %v", err)
		return
	}
	for _, currency := range listCurrency {
		resp := MakeRequest(url, currency.Name)
		for _, el := range resp.Values {
			err = pg.CreatePriceOfTransactions(repository.PriceOfTransactions{
				Name:  resp.Name,
				Price: el.Price,
				Data:  el.Data,
			})
			switch err {
			case sql.ErrNoRows:

			default:
				if err != nil {
					log.Fatalf("Error with create record to transaction: %v", err)
				}
			}
		}

	}
}

func GetAllCurrencyPairs(url string) []string {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
		return nil
	}
	defer resp.Body.Close()

	var result map[string]interface{}

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		log.Fatalln(err)
		return nil
	}
	var listCP []string
	for _, item := range result["currency"].([]interface{}) {
		listCP = append(listCP, item.(string))
	}
	return listCP
}
