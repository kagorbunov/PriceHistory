package repository

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"strings"
	"time"
)

type Pg struct {
	db *sql.DB
}

func NewPg(user, password, dbname, addr string) *Pg {
	listAddr := strings.Split(addr, ":")
	connectionString :=
		fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
			listAddr[0], listAddr[1], user, password, dbname)

	db, err := sql.Open("postgres", connectionString)
	if err != nil {
		log.Fatal(err)
	}
	return &Pg{db: db}
}

type CurrencyPairs struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

func (pg *Pg) GetAllCurrencyPairs() ([]CurrencyPairs, error) {
	rows, err := pg.db.Query("SELECT id, name_pair FROM currency")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	cPairs := []CurrencyPairs{}

	for rows.Next() {
		var cPair CurrencyPairs
		if err = rows.Scan(&cPair.ID, &cPair.Name); err != nil {
			return nil, err
		}
		cPairs = append(cPairs, cPair)
	}

	return cPairs, nil
}

func (pg *Pg) UpdateCurrencyPairs(cPair CurrencyPairs) error {
	_, err :=
		pg.db.Exec("UPDATE currency SET name_pair=$1 WHERE id=$2",
			cPair.Name, cPair.ID)

	return err
}

func (pg *Pg) DeleteCurrencyPairs(cPair CurrencyPairs) error {
	_, err := pg.db.Exec("DELETE FROM currency WHERE ID=$1", cPair.ID)

	return err
}

func (pg *Pg) CreateCurrencyPairs(cPair CurrencyPairs) (string, error) {
	err := pg.db.QueryRow(
		"INSERT INTO currency(name_pair) VALUES($1) ON CONFLICT DO NOTHING RETURNING ID",
		cPair.Name).Scan(&cPair.ID)

	if err != nil {
		return "0", err
	}

	return cPair.ID, nil
}

type PriceOfTransactions struct {
	ID    string `json:"id"`
	Name  string `json:"name"`
	Data  string `json:"data"`
	Price string `json:"price"`
}

func (pg *Pg) GetPriceOfTransactions(name string) ([]PriceOfTransactions, error) {
	rows, err := pg.db.Query("SELECT id, nameTransaction, dataTransaction, price FROM transaction WHERE nameTransaction=$1",
		name)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	listPT := []PriceOfTransactions{}

	for rows.Next() {
		var pt PriceOfTransactions
		if err = rows.Scan(&pt.ID, &pt.Name, &pt.Data, &pt.Price); err != nil {
			return nil, err
		}
		listPT = append(listPT, pt)
	}

	return listPT, nil
}

func (pg *Pg) UpdatePriceOfTransactions(pt PriceOfTransactions) error {
	_, err :=
		pg.db.Exec("UPDATE transaction SET nameTransaction=$1,dataTransaction=$2,price=$3 WHERE id=$4",
			pt.Name,
			pt.Data,
			pt.Price,
			pt.ID)

	return err
}

func (pg *Pg) DeletePriceOfTransactions(pt PriceOfTransactions) error {
	_, err := pg.db.Exec("DELETE FROM transaction WHERE ID=$1", pt.ID)

	return err
}

func (pg *Pg) CreatePriceOfTransactions(pt PriceOfTransactions) error {
	err := pg.db.QueryRow(
		"INSERT INTO transaction(nameTransaction,dataTransaction,price) VALUES($1,$2,$3) ON CONFLICT (nameTransaction, dataTransaction) DO NOTHING",
		pt.Name,
		pt.Data,
		pt.Price,
	).Scan(&pt.ID)

	if err != nil {
		return err
	}

	return nil
}

func (pg *Pg) GetPriceOfTransactionsInPeriod(name string, start, stop time.Time) ([]PriceOfTransactions, error) {
	allPT, err := pg.GetPriceOfTransactions(name)
	if err != nil {
		return nil, err
	}
	listPT := []PriceOfTransactions{}
	for _, pt := range allPT {
		data, err := time.Parse(time.Stamp, pt.Data)
		if err != nil {
			return nil, err
		}
		if data.After(start) && data.Before(stop) {
			listPT = append(listPT, pt)
		}
	}
	return listPT, nil
}
