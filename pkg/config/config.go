package config

func New() *Config {
	return &Config{
		Router: &Router{
			Addr: getEnv("ROUTER_ADDR", "localhost:8080"),
		},
		DB: &DataBase{
			Username: getEnv("DB_USERNAME", ""),
			Password: getEnv("DB_PASSWORD", ""),
			DBName:   getEnv("DB_NAME", ""),
			Addr:     getEnv("DB_ADDR", ""),
		},
		CurrencyPairs: &CurrencyPairs{
			Addr:    getEnv("CP_ADDR", ""),
			AddrAll: getEnv("CP_ADDR_ALL", ""),
			Timeout: getEnv("CP_TIMEOUT", "5m"),
		}}
}

type Config struct {
	Router        *Router
	DB            *DataBase
	CurrencyPairs *CurrencyPairs
}

type Router struct {
	Addr string
}

type DataBase struct {
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
	DBName   string `json:"dbname,omitempty"`
	Addr     string `json:"Addr,omitempty"`
}

type CurrencyPairs struct {
	Addr    string
	AddrAll string
	Timeout string
}
