package manager

import (
	"database/sql"
	"fmt"
	"gitlab.com/kagorbunov/PriceHistory/pkg/handler"
	"gitlab.com/kagorbunov/PriceHistory/pkg/repository"
	"log"
)

type Manager struct {
	pg       *repository.Pg
	urlAllCP string
}

func NewManager(pg *repository.Pg, urlAllCP string) *Manager {
	return &Manager{pg: pg, urlAllCP: urlAllCP}
}

func (m *Manager) CreateCurrencyPairs() error {
	listCP := handler.GetAllCurrencyPairs(m.urlAllCP)
	fmt.Println(listCP)
	for _, cp := range listCP {
		_, err := m.pg.CreateCurrencyPairs(repository.CurrencyPairs{Name: cp})
		switch err {
		case sql.ErrNoRows:
			return nil
		default:
			if err != nil {
				log.Fatalf("Error with set currency pairs: %v", err)
				return err
			}
		}
	}
	return nil
}
