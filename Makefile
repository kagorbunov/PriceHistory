run:
	go run main.go

postgres:
	docker-compose up

migration:
	go install github.com/pressly/goose/v3/cmd/goose@latest
	cd migration
	goose postgres "user=postgres password=db_user sslmode=disable" up

lint:
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin

lint-run: lint
	golangci-lint run
