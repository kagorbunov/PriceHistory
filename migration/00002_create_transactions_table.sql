-- +goose Up
-- +goose StatementBegin
CREATE TABLE transaction(
                          id serial PRIMARY KEY,
                          nameTransaction varchar(255) NOT NULL,
                          dataTransaction varchar(255) NOT NULL,
                          price varchar(255) NOT NULL
);
-- +goose StatementEnd
-- +goose StatementBegin
CREATE UNIQUE INDEX idx_name_data ON transaction (nameTransaction, dataTransaction);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE transaction;
-- +goose StatementEnd

