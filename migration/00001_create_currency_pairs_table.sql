-- +goose Up
-- +goose StatementBegin
CREATE TABLE currency(
    id serial PRIMARY KEY,
    name_pair varchar(255) NOT NULL UNIQUE
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE currency;
-- +goose StatementEnd
